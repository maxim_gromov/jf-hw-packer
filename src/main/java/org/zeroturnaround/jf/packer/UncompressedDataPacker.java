package org.zeroturnaround.jf.packer;


import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.toList;

public class UncompressedDataPacker implements Packer {

    private static final int ARCHIVE_TYPE_BYTE = 42;
    private static final int BUFFER_SIZE = 4096;

    private static final Logger log = LoggerFactory.getLogger(UncompressedDataPacker.class);

    @Override
    public void pack(Path inputDir, Path outputArchive) throws IOException {
        log.info("Packing {} into {}", inputDir, outputArchive);
        this.writeInputToArchive(inputDir, outputArchive);
    }

    private void writeInputToArchive(Path inputDir, Path outputArchive) throws IOException {
        this.validateAndCreateDirectories(inputDir, outputArchive, false);

        List<Path> fileChunks = FileUtils.listFiles(inputDir.toFile(), null, true)
                .stream()
                .map(File::toPath)
                .filter(path -> !Files.isDirectory(path))
                .collect(toList());

        try (DataOutputStream out = new DataOutputStream(new BufferedOutputStream(Files.newOutputStream(outputArchive)))) {
            this.writeFileChunksToArchive(inputDir, fileChunks, out);
        }
    }

    @Override
    public void unpack(Path inputArchive, Path outputDir) throws IOException {
        log.info("Unpacking {} into {}", inputArchive, outputDir);
        this.writeArchiveToOutput(inputArchive, outputDir);
    }

    private void writeArchiveToOutput(Path archiveDir, Path outputDir) throws IOException {
        this.validateAndCreateDirectories(archiveDir, outputDir, true);

        try (DataInputStream in = new DataInputStream(new BufferedInputStream(Files.newInputStream(archiveDir)))) {
            final byte archiveType = in.readByte();

            if (ARCHIVE_TYPE_BYTE != archiveType) {
                throw new IOException("Illegal archive format");
            }

            this.writeOutputFilesRecursively(outputDir, in);
        }
    }

    private void writeOutputFilesRecursively(Path outputDir, DataInputStream inputStream) throws IOException {
        if (inputStream.available() == 0)
            return;

        Path filePath = readAndCreateOutputPath(outputDir, inputStream);
        long fileSize = inputStream.readLong();
        final int bufferSize = fileSize >= BUFFER_SIZE ? BUFFER_SIZE : (int) fileSize;

        try (DataOutputStream out = new DataOutputStream(new BufferedOutputStream(Files.newOutputStream(filePath)))) {
            int length;
            byte[] buffer = new byte[bufferSize];

            while (fileSize != 0 && (length = inputStream.read(buffer)) != -1) {
                fileSize -= length;
                out.write(buffer, 0, length);
                // if we have file size left to read < than buffer size, should read only last part, so change buffer size
                if (fileSize != 0 && fileSize < bufferSize) {
                    buffer = new byte[(int) fileSize];
                }
            }

            this.writeOutputFilesRecursively(outputDir, inputStream);
        }
    }

    private Path readAndCreateOutputPath(Path outputDir, DataInputStream inputStream) throws IOException {
        String filePathString = inputStream.readUTF();
        Path fileRelativePath = Paths.get(filePathString);
        Path filePath = outputDir.resolve(fileRelativePath);
        PackerUtils.createRecursivelyOutput(filePath, false);
        return filePath;
    }

    private void writeFileChunksToArchive(Path inputDir, List<Path> fileChunks, DataOutputStream outputStream) throws IOException {
        // Archive Type is 42 - 1 fixed byte
        outputStream.write(ARCHIVE_TYPE_BYTE);

        // Writing file chunks
        for (Path fileChunk : fileChunks) {
            final long fileSize = Files.size(fileChunk);
            int length;
            try (DataInputStream in = new DataInputStream(new BufferedInputStream(Files.newInputStream(fileChunk)))) {
                byte[] buffer = new byte[BUFFER_SIZE];
                Path fileRelativePath = inputDir.relativize(fileChunk);
                String formattedPath = fileRelativePath.toString().replace("\\", "/");

                outputStream.writeUTF(formattedPath);
                outputStream.writeLong(fileSize);
                while ((length = in.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, length);
                }
            }
        }
    }

    private void validateAndCreateDirectories(Path input, Path output, boolean isDirectory) throws IOException {
        final boolean inputExists = Files.exists(input);
        if (!inputExists || output == null) {
            throw new IOException("Input dir is not existing one or archive dir is null");
        } else {
            PackerUtils.createRecursivelyOutput(output, isDirectory);
        }
    }

}
