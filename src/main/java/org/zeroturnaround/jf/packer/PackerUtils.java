package org.zeroturnaround.jf.packer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class PackerUtils {

    public static void createRecursivelyOutput(Path outputDir, boolean isDirectory) throws IOException {
        if (outputDir != null) {
            Path outputDirParent = outputDir.getParent();
            // Create recursively parent directories
            if (!Files.exists(outputDirParent))
                Files.createDirectories(outputDirParent);
            // Create expected file or directory
            if (isDirectory) {
                Files.createDirectory(outputDir);
            } else {
                Files.createFile(outputDir);
            }
        }
    }

}
